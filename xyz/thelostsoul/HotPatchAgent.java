package xyz.thelostsoul;

import java.io.File;
import java.lang.instrument.Instrumentation;
import java.util.*;

/**
 * Created by jamie on 17-1-12.
 */
public class HotPatchAgent {

    public static void agentmain(String agentArgs, Instrumentation inst)
            throws Exception {
        main(agentArgs, inst);
    }

    private static synchronized void main(String agentArgs, Instrumentation inst)
            throws Exception {
        System.out.println("开始进行hotpatch操作,agentArgs参数:" + agentArgs);


        String[] args = agentArgs.split(" ");
        if ((args == null) || (args.length < 2)) {
            throw new Exception("必须传人绝对路径和类名");
        }

        Class[] appClasses = inst.getAllLoadedClasses();
        Map<String,Class> name_class = new HashMap<>();
        for(Class appClass:appClasses){
            name_class.put(appClass.getName().trim(),appClass);
        }

        File f = new File(args[0]);
        if ((f.isFile()) || (f.isDirectory())) {
            List<Class> classes = new ArrayList<>();
            for (int i = 1; i < args.length; i++) {
                if (args[i]!=null) {
                    ClassLoader item = null;
                    Class loadedClass = name_class.get(args[i].trim());
                    if(loadedClass==null){
                        throw new Exception(args[i].trim()+":该类未加载");
                    }
                    item = loadedClass.getClassLoader();

                    Class clazz = null;
                    try {
                        clazz = Class.forName(args[i].trim(), false, item);
                    } catch (ClassNotFoundException ex) {
                        throw new Exception("对应的ClassLoader:" + item.getClass().getName() + "@" + item.hashCode() + "," + item + ",类名:" + args[i] + "对应的类无法找到:"+ ex);
                    }
                    if (clazz != null) {
                        classes.add(clazz);
                    }
                } else {
                    System.out.println("类名:" + args[i] + "为空");
                }
            }
            if (!classes.isEmpty()) {
                System.out.println("需要进行" + classes.size() + "个类的hotpatch");

                HotPatchTransformer objHotPatchTransformer = new HotPatchTransformer(args[0], classes.toArray(new Class[0]));
                try {
                    inst.addTransformer(objHotPatchTransformer, true);
                    for (Class item : classes) {
                        inst.retransformClasses(new Class[]{item});
                        System.out.println("对应的ClassLoader:" + item.getClassLoader().getClass().getName() + "@" + item.getClassLoader().hashCode() + "," + item.getClassLoader() + ",类名:" + item.getName() + "完成hotpatch");
                    }
                    System.out.println("hotpatch结束");
                } finally {
                    inst.removeTransformer(objHotPatchTransformer);
                }
            } else {
                System.out.println("没有类需要进行hotpatch");
            }
        } else {
            throw new Exception("路径:" + args[0] + "无法找到");
        }
    }
}
