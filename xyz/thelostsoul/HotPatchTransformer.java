package xyz.thelostsoul;

import xyz.thelostsoul.util.HotPatchUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

/**
 * Created by jamie on 17-1-12.
 */
public class HotPatchTransformer implements ClassFileTransformer {
    private String path = null;
    private Class[] classes = null;

    public HotPatchTransformer(String path, Class[] classes)
    {
        this.path = path;
        this.classes = classes;
    }

    public byte[] getBytesFromFile(Class clazz)
    {
        if (this.path!=null) {
            try
            {
                File f = new File(this.path);
                if (f.isFile())
                {
                    if (f.getName().endsWith(".jar"))
                    {
                        JarInputStream jis = new JarInputStream(new BufferedInputStream(new FileInputStream(this.path)));
                        JarEntry jarEntry;
                        while ((jarEntry = jis.getNextJarEntry()) != null) {
                            if (!jarEntry.isDirectory())
                            {
                                String tmpStr = jarEntry.getName();
                                tmpStr = tmpStr.replace(".class", "");
                                tmpStr = tmpStr.replace("/", ".");
                                if (tmpStr.equals(clazz.getName()))
                                {
                                    JarFile jf = new JarFile(this.path);
                                    InputStream is = jf.getInputStream(jarEntry);

                                    System.out.println("从jar文件" + this.path + "/" + jarEntry.getName() + "中读取到class文件:" + clazz.getName());

                                    return HotPatchUtil.toByteArray(is);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("路径是文件但不是jar包,不进行hotpatch");
                    }
                }
                else
                {
                    if (f.isDirectory())
                    {
                        String tmpStr = clazz.getName();
                        tmpStr = tmpStr.replace(".", "/");
                        tmpStr = tmpStr + ".class";
                        FileInputStream fi = new FileInputStream(this.path + "/" + tmpStr);
                        System.out.println("从路径" + this.path + "/" + tmpStr + "中读取到class文件:" + clazz.getName());

                        return HotPatchUtil.toByteArray(fi);
                    }
                    throw new Exception("路径既不是文件也不是目录,不进行hotpatch");
                }
            }
            catch (Throwable ex)
            {
                System.out.println("读取class文件:" + clazz.getName() + "失败,打patch失败:"+ex);
                return null;
            }
        }
        return null;
    }

    public byte[] transform(ClassLoader l, String className, Class<?> c, ProtectionDomain pd, byte[] b)
            throws IllegalClassFormatException
    {
        if (this.classes != null) {
            for (int i = 0; i < this.classes.length; i++) {
                if (c.equals(this.classes[i]))
                {
                    System.out.println("类名:" + c.getName() + "进行transform");
                    return getBytesFromFile(this.classes[i]);
                }
            }
        }
        return null;
    }
}
