package xyz.thelostsoul.util;

import xyz.thelostsoul.HotPatchAgent;
import xyz.thelostsoul.HotPatchTransformer;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * Created by jamie on 17-1-12.
 */
public class HotPatchUtil {

    private static String HOTPATCH_TMP_DIR = null;
    private static String HOTPATCH_AGENT_JAR_PATH = null;

    static
    {
        String tmp = System.getProperty("hotpatch.tmpdir");
        if (tmp!=null)
        {
            HOTPATCH_TMP_DIR = tmp.trim() + "/hotpatch";
        }
        if (HOTPATCH_TMP_DIR==null) {
            HOTPATCH_TMP_DIR = System.getProperty("java.io.tmpdir") + "/hotpatch";
        }
        HOTPATCH_AGENT_JAR_PATH = HOTPATCH_TMP_DIR + "/agent";
    }

    private static String HOTPATCH_AGENT_JAR_FILE = HOTPATCH_AGENT_JAR_PATH+"/hp_agent.jar";

    public static boolean isJDK6(){
        double version = Double.parseDouble(System.getProperty("java.class.version"));
        return version >= 50.0D;
    }

    public static boolean isNumeric(String s){
        return s.trim().matches("\\d+");
    }

    public static void forceMkdir(File directory)
            throws IOException
    {
        if (directory.exists())
        {
            if (directory.isFile())
            {
                String message = "File " + directory + " exists and is " + "not a directory. Unable to create directory.";

                throw new IOException(message);
            }
        }
        else if (!directory.mkdirs())
        {
            String message = "Unable to create directory " + directory;

            throw new IOException(message);
        }
    }

    public static byte[] toByteArray(InputStream input)
            throws IOException
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        copy(input, output);
        return output.toByteArray();
    }

    public static int copy(InputStream input, OutputStream output)
            throws IOException
    {
        byte[] buffer = new byte[4096];
        int count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer)))
        {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    public static void makeHotPatchAgentJar()
            throws Exception
    {
        File f = new File(getHotpatchAgentJarPath());
        if (!f.exists()) {
            forceMkdir(f);
        }
        Manifest manifest = new Manifest();
        Attributes attr = manifest.getMainAttributes();
        attr.putValue("Manifest-Version", "1.0");
        attr.putValue("Agent-Class", HotPatchAgent.class.getName());


        attr.putValue("Boot-Class-Path", "hp_agent.jar");
        attr.putValue("Can-Retransform-Classes", "true");
        attr.putValue("Can-Redefine-Classes", "true");

        List list = new ArrayList();
        list.add(HotPatchAgent.class);
        list.add(HotPatchTransformer.class);
        list.add(HotPatchUtil.class);

        JarOutputStream out = new JarOutputStream(new FileOutputStream(getHotpatchAgentJarFile()), manifest);
        for (Iterator iter = list.iterator(); iter.hasNext();)
        {
            Class item = (Class)iter.next();
            String name = item.getName();
            name = name.replace('.','/');
            name = name + ".class";
            out.putNextEntry(new JarEntry(name));
            out.write(toByteArray(Thread.currentThread().getContextClassLoader().getResourceAsStream(name)));
        }
        out.flush();
        out.close();
    }

    public static String getHotpatchAgentJarPath(){
        return HOTPATCH_AGENT_JAR_PATH;
    }

    public static String getHotpatchAgentJarFile() {
        return HOTPATCH_AGENT_JAR_FILE;
    }
}
