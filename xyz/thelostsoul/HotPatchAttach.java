package xyz.thelostsoul;

import com.sun.tools.attach.VirtualMachine;
import xyz.thelostsoul.util.HotPatchUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * Created by jamie on 17-1-12.
 */
public class HotPatchAttach {
    public static void main(String[] args)
            throws Exception
    {
        if (!HotPatchUtil.isJDK6())
        {
            System.out.println("JDK版本至少为jdk6");
            System.exit(-1);
        }
        if ((args == null) || (args.length < 3))
        {
            System.out.println("必须至少输入3个参数,第一个参数为PID,第二个参数为patch的路径,第三个参数为类名全称");
            System.exit(-1);
        }
        String pid = args[0];
        if (!HotPatchUtil.isNumeric(pid))
        {
            System.out.println("PID必须为数字");
            System.exit(-1);
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < args.length; i++)
        {
            sb.append(args[i]);
            if (i != args.length - 1) {
                sb.append(" ");
            }
        }
        HotPatchUtil.makeHotPatchAgentJar();

        VirtualMachine vm = VirtualMachine.attach(pid.trim());
        vm.loadAgent(HotPatchUtil.getHotpatchAgentJarFile(), sb.toString());
        vm.detach();
    }
}
